#HappyOrSad - TWITTER SENTIMENT
#22/09/2014
#james@finchcompany.com
#DESCRIPTION:
#This script attempts to infer the sentiment of a word
#leveraging the twitter search API and emoticons representative
#of binary emotion states :-) :-(
search_string = "tony abbot"

import os
import tweepy
smile_emots = [":)",":-)",":D",":-D","(:", "(-:",":]",":-]","[:", "[-:","#happy","U0001F603","U0001F604","U0001F606","U0001F606", "yay!", "happy", "good", "happiness"]
frown_emots = [":(",":-(","D:","D-:","):",")-:",":[",":-[","]:","]-:","#sad","U0001F612","U0001F614","U0001F61E","U0001F622","U0001F61C","U+1F61D","U0001F620", "sad", "bad", "crying"]
tweet_list = []
sample_count = sample_latch = smile_score = frown_score = 0

##"TOP SECRET" API STUFF##
#(feel free to steal my twitter account)#
consumer_key = "5L676Djfg7vuIDG5GlPXkQ"
consumer_secret = "EKh3s2VWE8VSmqHe4GllF9U91mZRsFiuTT3qWPc"
access_token = "2337418333-9S2gDUVIagW1soGYAl3cGX3MgblwjCtfm3nJaow"
access_token_secret = "tTjAorsMfeqeImcOYVroCaYD906hYkUFEXG2J78TaleBM"

#api setup
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
#api suthorise
api = tweepy.API(auth)
#search for tweets with the string
for tweet in tweepy.Cursor(api.search,
                           search_string,
                           rpp=100,
                           result_type="recent",
                           include_entities=True,
                           lang="en").items():
    tweet_list.append(tweet.text)
    os.system('say {!s}'.format(tweet.text))
    #print tweet_list
    for i in range(0, len(tweet_list)):
        for j in range(0, len(smile_emots)):
            if tweet_list[i].find(smile_emots[j]) != -1:
                smile_score = smile_score+1
                sample_count = sample_count+1
                #print tweet_list[i]
        for k in range(0, len(frown_emots)):
            if tweet_list[i].find(frown_emots[k]) != -1:
                frown_score = frown_score+1
                sample_count = sample_count+1
                #print tweet_list[i]
        if sample_count > sample_latch:
            happy_per = 100*(smile_score/(smile_score+frown_score+0.001))
            sad_per = 100*(frown_score/(smile_score+frown_score+0.001))
            print("The word <" + search_string + "> has been determined to be{:10.4f}% Happy {:10.4f}% Sad from analysis of ".format(happy_per, sad_per) + str(sample_count) + " Tweets")
            sample_latch = sample_count
