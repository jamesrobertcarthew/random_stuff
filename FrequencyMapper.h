//
//  FrequencyMapper.h
//  Audio Tool
//  Provides a reference of audio frequencies based off piano key notes
//  Created by James Carthew on 9/12/2014.
//  Copyright (c) 2014 James Carthew. All rights reserved.
//

#import <Foundation/Foundation.h>

#define A0 27.5
#define AS0 29.135
#define B0 30.868
#define C1 32.703
#define CS1 34.648
#define D1 36.708
#define DS1 38.891
#define E1 41.203
#define F1 43.654
#define FS1 46.249
#define G1 48.999
#define GS1 51.913
#define A1 55.000
#define AS1 58.270
#define B1 61.735
#define C2 65.406
#define CS2 69.296
#define D2 73.416
#define DS2 77.782
#define E2 82.407
#define F2 87.307
#define FS2 92.499
#define G2 97.999
#define GS2 103.83
#define A2 110.00
#define AS2 116.54
#define B2 123.47
#define C3 130.81
#define CS3 138.59
#define D3 146.83
#define DS3 155.56
#define E3 164.82
#define F3 174.61
#define FS3 185.00
#define G3 196.00
#define GS3 207.65
#define A3 220.00
#define AS3 233.08
#define B3 246.94
#define C4 261.63
#define CS4 277.18
#define D4 293.67
#define DS4 311.13
#define E4 329.63
#define F4 349.23
#define FS4 369.99
#define G4 392.00
#define GS4 415.30
#define A4 440.00
#define AS4 466.16
#define B4 493.88
#define C5 523.25
#define CS5 554.37
#define D5 587.33
#define DS5 622.25
#define E5 659.26
#define F5 698.46
#define FS5 739.99
#define G5 783.99
#define GS5 830.61
#define A5 880.00
#define AS5 932.33
#define B5 987.77
#define C6 1046.5
#define CS6 1108.7
#define D6 1174.7
#define DS6 1244.5
#define E6 1318.5
#define F6 1396.9
#define FS6 1480.0
#define G6 1568.0
#define GS6 1661.2
#define A6 1760.0
#define AS6 1864.7
#define B6 1975.5
#define C7 2093.0
#define CS7 2217.5
#define D7 2349.3
#define DS7 2489.0
#define E7 2637.0
#define F7 2793.0
#define FS7 2960.0
#define G7 3136.0
#define GS7 3322.4
#define A7 3520.0
#define AS7 3729.3
#define B7 3951.1
#define C8 4186.0
@interface FrequencyMapper : NSObject

@end
