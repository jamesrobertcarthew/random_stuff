# random_stuff
a collection of random stuff that follows me around
- [Frequency Mapper:](https://github.com/jamesrobertcarthew/random_stuff/blob/master/FrequencyMapper.h) note > frequencies
- [HappyOrSad:](https://github.com/jamesrobertcarthew/random_stuff/blob/master/HappyOrSad.py) really dumb sentiment analysis, using Twitter
- [qc:](https://github.com/jamesrobertcarthew/random_stuff/blob/master/qc.py) quick multi-paragraph chunk commenting with word wrap for python and C delivered to your Clipboard!
